import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { DocumentComponent } from './document/document.component';
import { SignupComponent } from './signup/signup.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { SurveyComponent } from './dashboard/survey/survey.component';
import { LogoutComponent } from './dashboard/logout/logout.component';
import { PublicSurveyComponent } from './survey/survey.component';
import { FormatSurveyComponent } from './survey/format.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    DashboardComponent,
    DocumentComponent,
    SignupComponent,
    ProfileComponent,
    SurveyComponent,
    LogoutComponent,
    PublicSurveyComponent,
    FormatSurveyComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'documentation',
        component: DocumentComponent
      },
      {
        path: 'signup',
        component: SignupComponent
      },
      {
        path: 'survey',
        component: PublicSurveyComponent
      },
      {
        path: 'dashboard/profile',
        component: ProfileComponent
      },
      {
        path: 'dashboard/survey',
        component: SurveyComponent
      },
      {
        path: 'dashboard/logout',
        component: LogoutComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
