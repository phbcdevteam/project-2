import { Component, OnInit } from '@angular/core';
import * as Survey from 'survey-angular';
import * as $ from 'jquery';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class PublicSurveyComponent implements OnInit {

  constructor() { }
  json = {
    questions: [{
      type: "matrixdynamic",
      addRowText: "Add language",
      columns: [{
        name: "Language",
        choices: ["Javascript", "Java", "Python", "CSS", "PHP", "Ruby", "C++", "C", "Shell", "C#", "Objective-C", "R", "VimL", "Go", "Perl", "CoffeeScript", "TeX", "Swift", "Scala", "Emacs List", "Haskell", "Lua", "Clojure", "Matlab", "Arduino", "Makefile", "Groovy", "Puppet", "Rust", "PowerShell"],
        cellType: "dropdown",
        isRequired: true,
        hasOther: true,
        choicesOrder: "asc"
      }, {
        name: "Experience",
        choices: ["One year or less", "From one two three years", "From three to five years", "More then five years"],
        cellType: "dropdown",
        isRequired: true
      }],
      isRequired: true,
      name: "languages",
      rowCount: 1,
      title: "What programming languages do you know?"
    }]
  };
  ngOnInit() {
  }

}
